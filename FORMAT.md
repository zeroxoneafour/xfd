# XFD FORMAT
### HISTORY
XFD, or eXtensible File Database, was originally titled XDF or eXpandable Database Format. It was documented roughly in a standard ruled page in my binder. I will now describe it in more detail.
### BASICS
XFD uses operators to describe parts of a "file". A "file" is not a actual file, but instead a collection of operators. A file can have any mismatching of standard, namespace, or custom operators (more on that later). The true file, appended with the file extension .xfd, is a collection of these "files". These "files" are called files and not collections, or something else, because XFD was originally a format built for compacting and archiving binary files.
### OPERATORS
An operator in XFD uses the following format-   
`<namespace><name><size of operator contents><NULL(0x00)><contents>`  
Lets break this down.  
`namespace` is the "mode" of the operator. It can have 3 values- c(0x63), n(0x6E), or s(0x73). Always lowercase, the value is a different hex if uppercase. `name` is the name of the operator. With standard (s namespace) and namespace (n namespace) operators, the name is _always_ exactly 3 bytes long. Custom namespaces have their own format for defining the operator, and are discussed in their own chapter. Their names can be infinitely long and therefore infinitely different/possible, checking out the "extensible" part of the name. `size of operator contents` declares the size of the section `contents` in the operator. It is a number in base 255, a base like hexadecimal but not really. It will be discussed next chapter. `NULL` is obviously the hex character 0x00, and `contents` are the contents of the operator. The contents size must be equal to the `size of operator contents` section value, or the program will misread.
### WAIT, WHAT IS "BASE 255"?
To understand base 255, you need to know your basic hexadecimal. Hexadecimal is base 16. The third digit of base 16 is multiplied by 256 (16^2). Each character in base 255 is 1 byte long. We will describe it with hexadecimal.
A number in base 255 is a number in base 256, or a two digit base 16 number, but ommiting the `NULL` character. This means the first character in base 255, and the one that represents the character 0, is `0x01`. This means in order to find the number in base 10 (the standard base of mathematics/life), you-
* convert the 8-bit char to a hexadecimal (always two digits)
  + say you had the number AB, that would convert to `0x65` and `0x66`
* subtract 1 from each pair of hex numbers
  + `0x65` and `0x66` would become `0x64` and `0x65`
* stitch them together (that's not good wording, so see the example below)
  + `0x64` and `0x65` would become `0x6465`
* find the base 10 of that number
  + `0x6465` would become the number 25,701  

So, if the `size of operator contents` section had the value AB, the operator contents would be 25,701 bytes long.
### CUSTOM OPERATORS
Of course, sometimes you have a database with _many_ operators, so the 16,581,375 custom namespace operators you can have aren't enough. Bring in custom operators. Their `namespace` value is "c", as described above in the section "OPERATORS". Here is their format-  
`c<name><NULL><size of contents><NULL><contents>`  
`name` can be a string or a base 255 number, but it cannot contain `0x00`, or `NULL`. It can be as long as you want. After the first `NULL`, you declare the content size in base 255. After a `NULL` preceding that you declare the contents.
### FILES
A file is a collection of operators. Within files, you cannot have multiple of the same operator. You can, hovever, have the same operator in different files. Since the format is raw memory based, the format for files is just  
`<operator><operator><operator><operator>...`  
for as long or as short as you want. There is no character between operators, but to create a new file, put a `NULL` after the last operator in your file.
### SHARING DATA
Operators can share data amongst each other. They don't even have to be the same operator namespace (ex. `nEVJ` can share data with `cAdf@#DE<?dSF:fsUWfiAJf`). They can be in the same file, or different files. The syntax is-  
`<namespace and name><NULL><file number><NULL><operator in file>`  
`namespace and name` means if your operator is a namespace or standard operator, you put in n or s, then `name`. If it is a custom operator, you put in `c<name><NULL>`, so if it is a custom operator, you should have two `NULLS` in a row before the `file number`. The `file number` is the number of the file in sequential order. It is, of course, a base 255 number. The `operator in file` is the namespace and name of the operator in the file that you want to copy the data of. If the `operator in file` is custom, there will be a `NULL` at the end. The program will auto detect if the `NULL` is due to a custom operator, so don't worry about your file being terminated early!

This is it for now. Remember, new documentation is always being added, and PRs are always welcome. Thanks!